import taqiLogo from '../../assets/logos/taqi.png';

export const Taqi = {
    name: 'Taqi',
    logo: taqiLogo,
    colors: {
        primary: '#f39200',
        secondary: '#fff',
        success: '#5eba00',
        info: '#a0d911',
        warning: '#f1c40f',
        error: '#cd201f'
    },
    alerts: {
        primary: {
            color: '#849A1D',
            backgroud: '#e4eeb4',
            border: '#bbd632'
        },
        secondary: {
            color: '#464a4e',
            backgroud: '#e7e8ea',
            border: '#dddf&2'
        },
        success: {
            backgroud: '#f6ffed',
            border: '#b7eb8f'
        },
        info: {
            backgroud: '#e6f7ff',
            border: '#91d5ff'
        },
        warning: {
            backgroud: '#fffbe6',
            border: '#ffe58f'
        },
        error: {
            backgroud: '#fff2f0',
            border: '#ffccc7'
        }
    },
    layout : {
        header: {
            color: "#bbd632",
            background: 'gray'
        }
    }
}