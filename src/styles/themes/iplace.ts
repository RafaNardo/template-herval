import iplaceLogo from '../../assets/logos/iplace.png';

export const Iplace = {
    name: 'Iplace',
    logo: iplaceLogo,
    colors: {
        primary: '#bbd632',
        secondary: '#fff',
        success: '#5eba00',
        info: '#a0d911',
        warning: '#f1c40f',
        error: '#cd201f'
    },
    alerts: {
        primary: {
            backgroud: '#f4f8dd',
            border: '#bbd632'
        },
        secondary: {
            backgroud: '#F4F5F6',
            border: '#DDE0E3'
        },
        success: {
            backgroud: '#f6ffed',
            border: '#b7eb8f'
        },
        info: {
            backgroud: '#e6f7ff',
            border: '#91d5ff'
        },
        warning: {
            backgroud: '#fffbe6',
            border: '#ffe58f'
        },
        error: {
            backgroud: '#fff2f0',
            border: '#ffccc7'
        }
    },
    layout: {
        header: {
            color: "#bbd632",
            background: '#313131'
        },
        menu: {
            background: '#313131',
            color: '#ffffffa6',
            itens: {
                hoverColor: '#fff'
            }
        }
    }
}