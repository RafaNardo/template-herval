import hervalLogo from '../../assets/logos/herval.png';

export const Herval = {
    name: 'Herval',
    logo: hervalLogo,
    colors: {
        primary: '#c70000',
        secondary: '#fff',
        success: '#5eba00',
        info: '#a0d911',
        warning: '#f1c40f',
        error: '#cd201f'
    },
    alerts: {
        primary: {
            backgroud: '#e4eeb4',
            border: '#bbd632'
        },
        secondary: {
            backgroud: '#e7e8ea',
            border: '#dddf&2'
        },
        success: {
            backgroud: '#f6ffed',
            border: '#b7eb8f'
        },
        info: {
            backgroud: '#e6f7ff',
            border: '#91d5ff'
        },
        warning: {
            backgroud: '#fffbe6',
            border: '#ffe58f'
        },
        error: {
            backgroud: '#fff2f0',
            border: '#ffccc7'
        }
    },
    layout : {
        header: {
            color: "#bbd632",
            background: 'blue'
        }
    }
}