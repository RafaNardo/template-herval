import { Iplace, Taqi, Herval } from './themes'
import { ThemeProvider } from './providers/theme-provider'
import GlobalStyle from './global-style'


export const Themes = {
    Iplace,
    Taqi,
    Herval
}

export { ThemeProvider, GlobalStyle };