import { ThemeProvider as StyledThemeProvider } from 'styled-components'

export interface ITheme {
    name: string;
    logo: string;
    colors: {
        primary: string;
        secondary: string;
        success: string;
        info: string;
        warning: string;
        error: string;
    },
    alerts: {
        primary: {
            backgroud: string,
            border: string
        },
        secondary: {
            backgroud: string,
            border: string
        },
        success: {
            backgroud: string,
            border: string
        },
        info: {
            backgroud: string,
            border: string
        },
        warning: {
            backgroud: string,
            border: string
        },
        error: {
            backgroud: string,
            border: string
        }
    },
    layout: {
        header: {
            color: string,
            background: string
        }
    }
}

interface IThemeContextProps {
    theme: ITheme
}

export const ThemeProvider: React.FC<IThemeContextProps> = ({ children, theme }) => {
    return <StyledThemeProvider theme={theme}>
        {children}
    </StyledThemeProvider>
}