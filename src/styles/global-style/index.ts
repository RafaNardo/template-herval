import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle` 
    * {    
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        outline: 0;
        font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;
        font-feature-settings: "tnum";
        font-size: 14px;
        font-variant: tabular-nums;
        line-height: 1.5715;
    }

    h1 {
        margin-bottom: .5em;
        font-weight: 600;
        line-height: 1.23;
    }

    html, body {
        height: 100%
    }

    #root {
        color: #000000d9;
        height: 100%;
        max-height: 100%;
        display: flex;
        flex-direction: column;
    }
`;

export default GlobalStyle;