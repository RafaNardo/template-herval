import Pages  from '../pages';

export interface IRoute {
    component: () => JSX.Element,
    exact: boolean,
    path: string,
    menuText: string,
    title: string
}

const Routes: IRoute[] = [
    {
        component: Pages.Home,
        exact: true,
        path: "/",
        menuText: "Home",
        title: "Home"
    },
    {
        component: Pages.Avatars,
        exact: true,
        path: "/avatars",
        menuText: "Avatars",
        title: "Avatars"
    },
    {
        component: Pages.Cards,
        exact: true,
        path: "/cards",
        menuText: "Cards",
        title: "Apresentando Cards"
    },
    {
        component: Pages.Alerts,
        exact: true,
        path: "/alerts",
        menuText: "Alerts",
        title: "Apresentando Alerts"
    }
]

// TIPOS_DE_COTACOES: {
//     component: Pages.Listagem,
//     exact: true,
//     path: "/tipos-de-cotacoes",
//     title: "Tipos de cotações",
// },
// TIPOS_DE_COTACOES_EDICAO: {
//     component: Pages.TiposDeCotacoes.Edicao,
//     exact: true,
//     path: "/tipos-de-cotacoes/:id",
//     title: "Tipos de cotações"
// }

export default Routes;