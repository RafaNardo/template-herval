import styled from 'styled-components'
import { useTheme } from 'styled-components';
import { ITheme } from '../../../../styles/providers/theme-provider';

const StyledLogo = styled.img`
    width: 32px;
    height: 32px;
`;

const Logo = () => {
    const theme = useTheme() as ITheme;

    return <StyledLogo src={theme.logo}></StyledLogo>
}

export default Logo