import React from 'react'
import styled from 'styled-components'
import Space from "../../space";
import Logo from "./logo";
import Title from "./title";

interface HeaderProps {
    title: string;
}

const Wrapper = styled.header`
    height: 64px;
    min-height: 64px;
    max-height: 64px;
    line-height: 64px;
    padding: 0 1vh 0 1vh;
    display: flex;
    align-items: center;
    justify-content:space-between;

    ${props => props.theme.layout.header};
`;

const Header: React.FC<HeaderProps> = ({ title }) => {
    return (
        <Wrapper>
            <Space>
                <Logo />
                <Title>{title}</Title>
            </Space>
            <span>TTTT</span>
        </Wrapper>
    )
}

export default Header;