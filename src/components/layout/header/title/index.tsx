import styled from 'styled-components'

const Title = styled.h1`
    font-size: 1.3em;
    margin: 0;
`
export default Title