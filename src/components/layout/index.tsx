import styled from 'styled-components'
import Header from './header'
import Menu from './menu'
import Main from './main'
import Page from './page'
import MenuItem from './menu/menu-item'
import Routes from '../../routes'
import { Route } from 'react-router'

interface ILayoutProps {
    title: string;
}

const Wrapper = styled.section`
    height: 100%;
    display: flex;
    background-color: #e6e7e7;
`

const Layout: React.FC<ILayoutProps> = (props) => {
    return (
        <>
            <Header title={props.title}/>
            <Wrapper>
                <Menu>
                    {Routes.map((route, index) => {
                        return <MenuItem key={index} path={route.path}>{route.menuText}</MenuItem>
                    })}
                </Menu>
                <Main>
                    {Routes.map(({exact, path, title, component}, index) => {
                        return <Route key={index} path={path} exact={exact}>
                            <Page header={title} body={component()}/>
                        </Route> 
                    })}
                </Main>
            </Wrapper>
        </>
    )
}

export default Layout;