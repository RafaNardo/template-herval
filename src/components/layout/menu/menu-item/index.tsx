import { Link } from 'react-router-dom';
import styled from 'styled-components'

interface IMenuItemProps {
    path: string
}

const Wrapper = styled.li`
    &:hover {
        color: ${(props) => props.theme.layout.menu.itens.hoverColor}
    }
`;

const MenuItem : React.FC<IMenuItemProps> = ({path, children}) => {
    return <Wrapper>
        <Link to={path}>{children}</Link>
    </Wrapper>
}


export default MenuItem;