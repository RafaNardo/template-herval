import styled from 'styled-components'

const Wrapper = styled.ul`

`;

const SubMenu : React.FC = (children) => {
    return <Wrapper>
        { children }
    </Wrapper>
}


export default SubMenu;
