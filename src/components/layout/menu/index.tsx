import styled from 'styled-components'

const Aside = styled.aside`
    max-width: 210px;
    min-width: 210px;
    ${(props) => props.theme.layout.menu}
`;

const Ul = styled.ul`
    width: 100%;
    max-width: 100%;
    display: flex;
    flex-direction: column;
`;

const Menu: React.FC = ({children}) => {
    return <Aside>
        <Ul>
            {children}
        </Ul>
    </Aside>
}

export default Menu;