import styled from "styled-components";

const Content = styled.main`
    padding: 2vh;
    flex-grow: 2;
`
export default Content;