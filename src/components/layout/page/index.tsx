import styled from "styled-components";
import PageHeader from "./header"

interface IPageProps {
    header: string,
    body: JSX.Element
}

const Wrapper = styled.div`
    width: 100%;
    height: 100%;
    background-color: #fafafa;
    padding: 2vh;
    overflow: "auto"
`

const Page: React.FC<IPageProps> = ({header, body}) => {
    return (
        <Wrapper>
            <PageHeader>{header}</PageHeader>
            { body }
        </Wrapper>
    )
}

export default Page;