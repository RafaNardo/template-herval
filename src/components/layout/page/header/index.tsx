import styled from "styled-components";


const Wrapper = styled.div`
    padding: 5px 0px 5px 0px;
    margin-bottom: 15px;
    border-bottom: 1px solid #ebedf0;

    span {
        color: rgba(0, 0, 0, 0.85);
        font-weight: 600;
        font-size: 18px;
        line-height: 32px;
        white-space: nowrap;
        text-overflow: ellipsis;
    }
`;

const PageHeader: React.FC = ({children}) => {
    return (<Wrapper>
        <span>{children}</span>
    </Wrapper>)
}

export default PageHeader;