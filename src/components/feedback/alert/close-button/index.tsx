import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";

interface ICloseButtonProps {
    onClick: () => void
} 

const Button = styled.button`
    padding-left: 15px;
    border: none;
    cursor: pointer;
    background: transparent;
`

const CloseButton: React.FC<ICloseButtonProps> = (props) => {
    return (
        <Button {...props}>
            <FontAwesomeIcon icon={['fas', 'times']} />
        </Button>
    )
}

export default CloseButton;