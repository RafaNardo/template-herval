import styled from "styled-components";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

interface IAlertIconProps 
{
    icon?: any
}

const Wrapper = styled.i`
    padding-right: 15px;
`

const Icon: React.FC<IAlertIconProps> = (props) => {
    return (
        <Wrapper><FontAwesomeIcon icon={props.icon} /></Wrapper>
    )
}

export default Icon 