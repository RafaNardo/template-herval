import styled from "styled-components";
import { useState } from "react";
import CloseButton from './close-button'
import Icon from './icon'

interface IAlertProps {
    message: string,
    type?: 'primary' | 'secondary' | 'success' | 'info' | 'warning' | 'error',
    description?: string,
    align?: 'left' | 'center' | 'right',
    icon?: any,
    closable?: boolean,
    width?: string
}

const Wrapper = styled.div<IAlertProps>`
    background-color: ${props => props.theme.alerts[props.type ?? 'primary'].backgroud};
    border: 1px solid ${props => props.theme.alerts[props.type ?? 'primary'].border};
    border-radius: 2px;
    padding: 8px 15px;
    display: flex;
    align-items: flex-start;
    
    text-align: ${props => props.align ?? 'left'};

    width: ${props => props.width ?? 'auto'};
`

const WrapperData = styled.div`
    flex-grow: 1;
`
export const Alert: React.FC<IAlertProps> = (props) => {
    const [visible, setVisible] = useState(true)

    function handleClick() {
        setVisible(!visible)
    }

    if (visible) {
        return (
            <Wrapper {...props}>
                {props.icon && <Icon icon={props.icon}/>}
                <WrapperData>
                    {props.message}
                    {props.description && <p>{props.description}</p>}
                </WrapperData>
                {props.closable && <CloseButton onClick={handleClick} /> }
            </Wrapper>
        )
    }
    return (<></>)
}