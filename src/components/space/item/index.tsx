import styled from 'styled-components'

interface SpaceItemProps {
    margin: string
}

const Item = styled.div<SpaceItemProps>`
    display: inline-flex;
    align-items: center;

    margin-right: ${props => props.margin};
    margin-bottom: ${props => props.margin};
`
export default Item;