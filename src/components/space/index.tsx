import React from 'react'
import styled from 'styled-components'
import Item from './item'

interface SpaceProps {
    size?: 'small' | 'middle' | 'large',
    direction?: 'row' | 'row-reverse' | 'column' | 'column-reverse';
}

const getMargin = (size?: string) => {
    switch (size) {
        case 'middle':
            return '16px'
        case 'large':
            return '24px'
        default:
            return '8px'
    }
}

const Wrapper = styled.div<SpaceProps>`
    display: flex;
    flex-wrap: wrap;
    
    flex-direction: ${props => props.direction ? props.direction : "row"};
    align-items: ${props => props.direction?.includes("row") ? "center" : "flex-start"};
    
    margin-right: ${props => "-" + getMargin(props.size)};
    margin-bottom: ${props => "-" + getMargin(props.size)};
`;

const Space: React.FC<SpaceProps> = (props) => {
    const margin = getMargin(props.size)
    return (
      <Wrapper direction={props.direction} size={props.size}>
        {
            
            React.Children.map(props.children, (child, index) => {
                return <Item key={index} margin={margin}>{ child }</Item>
            })
        }
      </Wrapper>
    )
}

export default Space;