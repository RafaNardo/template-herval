import React from "react";
import styled from "styled-components";

export interface ICardHeaderProps
{
    title?: string
}

const Wrapper = styled.div`
    color: rgba(0,0,0,.85);
    font-size: 16px;
    font-weight: 500;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    border-bottom: 1px solid #f0f0f0;

    display: flex;
    justify-content: space-between;

`;

const Header: React.FC<ICardHeaderProps> = ({title, children}) => {
    return (<Wrapper>
        <span>{title}</span>
    </Wrapper>)
}

export default Header;