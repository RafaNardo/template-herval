import styled, { css } from "styled-components";
import Body from "./body";
import Header from "./header";


interface ICardProps {
    size?: 'sm' | 'lg',
    title?: string,
    headerOptions?: JSX.Element 
}

const getPadding = (size?: string) => {
    return size === 'sm' ? 
        css`padding: 8px 12px;` : 
        css`padding: 16px 24px;`
}

const CardWrapper = styled.div<ICardProps>`
    background: #fff;
    border: 1px solid #f0f0f0;
    box-sizing: border-box;
    font-feature-settings: "tnum";
    font-size: 14px;
    font-variant: tabular-nums;
    line-height: 1.5715;
    list-style: none;
    margin: 0;
    padding: 0;
    position: relative;

    display: flex;
    flex-direction: column;

    div {
        ${ props => getPadding(props.size) }
    }
`

export const Card: React.FC<ICardProps> = (props) => {
    return (
        <CardWrapper {...props}>
            <Header title={props.title}/>
            <Body>
                { props.children }
            </Body>
        </CardWrapper>
    )
}