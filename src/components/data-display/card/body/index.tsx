import styled from "styled-components";

const Body = styled.div`
    flex-grow: 1;
`;

export default Body;