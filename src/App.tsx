import { GlobalStyle, Themes, ThemeProvider } from './styles'
import { Layout } from './components'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { fas } from '@fortawesome/free-solid-svg-icons'

import {
  BrowserRouter as Router
} from "react-router-dom";

function App() {
  library.add(fab, fas)

  return (
    <Router>
      <GlobalStyle />
      <ThemeProvider theme={Themes.Iplace}>
        <Layout title="Corporativo Iplace" />
      </ThemeProvider>
    </Router>
  );
}

export default App;
