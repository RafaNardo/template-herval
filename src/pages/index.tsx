import Home from './home'
import Avatars from './avatars'
import Cards from './cards'
import Alerts from './alerts'

const Pages = {
    Home,
    Avatars,
    Cards,
    Alerts
}

export default Pages