import { Card } from '../../components/data-display'

const teste = () => {
    return <>
        <a href="blablabla">Link</a>
        <button>Btn</button>
    </>
}

const Cards = () => { 
    return (
        <Card title="Cardzão Bolado" headerOptions={teste()}>
            <p>Body</p>
            <p>Body</p>
            <p>Body</p>
            <p>Body</p>
            <p>Body</p>
        </Card>
    )
};

export default Cards;