import { Alert } from '../../components/feedback'
import Space from '../../components/space'

const Alerts = () => {
    return (
        <Space direction="column">
            <Alert message="primary" align="center" type="primary" description="Descrição adicional para mensagem de alerta."/>
            <Alert message="secondary" align="right" type="secondary" description="Descrição adicional para mensagem de alerta."/>
            <Alert message="success" align="left" icon={['fas', 'check']} type="success" description="Descrição adicional para mensagem de alerta."/>
            <Alert message="info" align="center" icon={['fas', 'exclamation']}  type="info" />
            
            <Alert 
                closable
                width="100%"
                message="warning warning warning warning warning warning warning warning warning warning warning warning warning warning " 
                align="center" 
                icon={['fas', 'exclamation-triangle']} 
                type="warning" />
            
            <Alert
                width="300px"
                icon={['fas', 'times']} 
                type="error" 
                message="Error Text"
                description="Error Description Error Description Error Description Error Description Error Description Error Description"/>
        </Space>
    )
};

export default Alerts;