# Estrutura do projeto React Native do Supera

## Objetivo

Mostrar estrutura utilizada no aplicativo do Supera:

-  Organização de código e estrutura de pastas
- Bibliotecas utilizadas
  - StyledComponents
  - React Query
  - Axios
  - Unform
  - Yup
- Gerenciamento de contexto
- TypeScript
- Testes unitários (Jest e React Test Renderer)
- Eslint e Prettier

## Organização de código e estrutura de pastas

``` bash
src/
__api/
____queries/ # hooks de consultas sem side effect na API (GET ou POST)
____mutations/ # hooks de consultas com side effect na API (POST, PUT, DELETE e etc)
____services/ # integração com API literalmente (Axios)
__components/ # componentes genéricos
__contexts/ # contextos utilizados pela aplicação (Context API)
__pages/
____home/
______components/ # componentes específicos da página
______index.ts
__routes/ # rotas/stacks do React Navigation
__storage/ # integração para persistir informações no device
__styles/
____global-style/ # Configuração do estilo global do app
____providers/ # provider de temas
____themes/ # configuração de temas do app
__types/ # modelos de domínio
__utils/ # utilitários
```

## Bibliotecas utilizadas

- [StyledComponents](https://styled-components.com): estilização de componentes
- [React Query](https://react-query.tanstack.com): sincronização de dados com servidor: abstrai consultas, cache e atualização das informações.
- [Axios](https://github.com/axios/axios): cliente HTTP baseado em *Promise*
- [Unform](https://unform.dev/): controle de formulários
- [Yup](https://github.com/jquense/yup): validação de schemas de dados

## Gerenciamento de contexto

Não estamos utilizando Redux, por entendemos que não valia a pena e traria mais complexidade do que ganhos.

Estamos utilizando um recurso do próprio React que é a [Context API](https://pt-br.reactjs.org/docs/context.html#when-to-use-context) para armazenar algum estado global do app, como por exemplo, o usuário logado.

## TypeScript

* Tipagem de props e state dos componentes
* Melhora navegação
* Refatoração mais fácil
* Diminui quantidade de bugs

## Testes unitários

* Disseminando a cultura de testes unitários
* Principalmente para os componentes genéricos

## Eslint e prettier

Validação estática, padrões e formatação de código.

* Eslint
  * Usando o template do Airbnb
  * Com algumas customizações
* Prettier

## Referências

- [Artigo: Double Your React Coding Speed With This Simple Trick](https://link.medium.com/o0nCZJkFzdb)
